let React = require('react');
let settings = require('./settings.js');

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}


var App = React.createClass({
  getInitialState: function() {
    return {
      videos: []
    };
  },

  render: function() {
    return <div>
      <button onClick={this.handleLoginButtonClick}>
        Login
      </button>
      <div style={{width: 1100, height: 1000}}>
        <div style={{display: 'inline-block', width: 800, verticalAlign: 'top'}}>
          <div id="player" />
        </div>
        <div style={{display: 'inline-block', width: 200}}>
          {this.state.videos.map(function(video) {
            return (
              <div 
                  style={{backgroundColor: 'salmon', margin: 6}}
                  onClick={function() {
                    window.player.loadVideoById(video.resourceId.videoId);
                  }}>
                {video.title}
              </div>
            );
          })}
        </div>
      </div>
    </div>;
  },

  handleLoginButtonClick: function() {
    let self = this;

    gapi.auth.authorize({
      client_id: settings.clientId,
      scope: [
        'https://www.googleapis.com/auth/youtube'
      ],
      immediate: false
    }, function() {
      gapi.client.load('youtube', 'v3').then(function() {
        window.gapi = gapi;
        gapi.client.setApiKey(settings.apiKey);
        gapi.client.load('youtube', 'v3').then(function() {
          var videos = [];

          function getPage(pageToken) {
            console.log("getting page", pageToken);
            gapi.client.youtube.playlistItems.list({
              playlistId: settings.playlistId, 
              part: 'snippet',
              maxResults: 50,
              pageToken: pageToken
            }).then(function(response) {
              videos = videos.concat(response.result.items.map(function(item) {
                return item.snippet; //.resourceId.videoId;
              }));
              if (videos.length < response.result.pageInfo.totalResults) {
                getPage(response.result.nextPageToken);
              }
              else {
                var shuffledVideos = shuffle(videos);

                window.onYouTubeIframeAPIReady = function() {
                  var player = window.player = new YT.Player('player', {
                    height: '390',
                    width: '640',
                    videoId: videos[0].resourceId.videoId,
                    events: {
                      'onReady': function() {
                      },
                      'onStateChange': function() {
                      } 
                    }
                  });
                };

                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                self.setState({videos: videos});
                window.videos = videos;
                console.log("finished!");
              }
            });
          };

          getPage();
        });
      });
    });
  }
});


$(document).ready(function() {
  React.render(<App />, document.getElementById("app_root"));
});


