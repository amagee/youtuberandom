var webpack = require('webpack');

var config = {
  entry: {
    coincraft: './src/index.js'
  },
  output: {
    filename: './build/index.js'
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react'],
          plugins: [
            'transform-object-rest-spread',
            
            // This makes static class variables work in IE10.
            'transform-proto-to-assign'
          ]
        }
      }
    ]
  }
};

config.devtool = 'cheap-source-map';

module.exports = config;
